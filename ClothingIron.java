public class ClothingIron{
	private int temperature;
	public void setTemp(int newTemperature){
		this.temperature=newTemperature;
	}
	public int getTemp(){
		return this.temperature;
	}
	private int weight;
	public void setWeight(int newWeight){
		this.weight=newWeight;
	}
	public int getWeight(){
		return this.weight;
	}
	//Most clothing irons have a switch to decide how how the iron should be
	private int levelOfHeat;
	public void setHeat(int newLevelOHeat){
		this.levelOfHeat=newLevelOHeat;
	}
	public int getHeat(){
		return this.levelOfHeat;
	}
	public ClothingIron(int newWeight, int newTemperature, int newLevelOfHeat){
		this.weight = newWeight;
		this.temperature = newTemperature;
		this.levelOfHeat=newLevelOfHeat;
	}
	//should have added a 4th temp for this to work correclty bu heh, its just homework
	private int changeTemp(int levelOfHeat, int temperature){
		if(levelOfHeat ==1){
			return (int)temperature/3;
		}else if(levelOfHeat ==2){
			return (int)temperature/2;
		}else if(levelOfHeat ==3){	
			return (int)temperature* 1;
		}else{
			return (int)temperature*0;
		}
	}
	public void setChangeTemp(int levelHeat, int newTemperature){
		this.levelOfHeat=levelHeat;
		this.temperature=newTemperature;
	}
	public int getChangeTemp(){
		return this.changeTemp(this.levelOfHeat,this.temperature);
	}
	
	private boolean ironClothes(int weight, int temperature){
		if(weight >50 & temperature>55){
		System.out.println("Clothes have been ironed");
		return true;
		}else{
			System.out.println("Clothes have NOT been ironed");
			return false;
		}
	}
	public void setIronClothes(int newWeight,int newTemperature){
		this.weight=newWeight;
		this.temperature=newTemperature;
	}
	public boolean getIronClothes(){
		return this.ironClothes(weight,temperature);
	}
	private int recharge(int LevelOfHeat,double weight){
		System.out.println("Recharging wireless clothing iron, resetting level of heat to 0, the heavier the weight of the appliance, the longer the time to recharge fully");
		double timeToRecharge = (this.weight*10)/6;
		if(timeToRecharge<=50){
			for(int i=0;i<5;i++){
				System.out.println("...");
			}
		}else if(timeToRecharge>50 && timeToRecharge<=200){
			for(int k=0;k<10;k++){
				System.out.println(".....");
			}
		}else{
			System.out.println("You're gonna be here for a while");
			for(int l=0;l<15;l++){
				System.out.println(".......");
				if(l==10){
					System.out.println("... Still charging ...");
				}
			}
		}
		System.out.println("It took " + timeToRecharge +" minutes to recharge fully");
		return this.levelOfHeat = 0;
	}
	public void setRecharge(int levelHeat,int newWeight){
		this.levelOfHeat = levelHeat;
		this.weight=newWeight;
	}
	public int getRecharge(){
		return this.recharge(this.levelOfHeat,this.weight);
	}
}