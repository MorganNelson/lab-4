import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		ClothingIron[] appliances = new ClothingIron[4];
		for(int i=0;i<appliances.length;i++){
			System.out.println("appliance " +i);
			System.out.println("Enter the maximum temperature that your appliance can reach");
			int maxTemp = input.nextInt();
			System.out.println("Enter the weight in pounds of your appliance");
			int weightClothingIron= input.nextInt();
			int currentLevelOfHeat=0;
			do{
			System.out.println("Enter the current level of heat of your appliance, it go to a maximum of 3 and a minimum of 0");
			currentLevelOfHeat=input.nextInt();}while(currentLevelOfHeat>3 || currentLevelOfHeat<0);
			appliances[i]= new ClothingIron(weightClothingIron,maxTemp,currentLevelOfHeat);
		}
		System.out.println("Before: " +appliances[3].getWeight()+","+appliances[3].getTemp()+","+appliances[3].getHeat());
		appliances[3].setWeight(input.nextInt());
		appliances[3].setTemp(input.nextInt());
		appliances[3].setHeat(input.nextInt());
		System.out.println("After	: " +appliances[3].getWeight()+","+appliances[3].getTemp()+","+appliances[3].getHeat());
		//System.out.println("Temperature " + appliances[3].temperature +" , " + "Weight " + appliances[3].weight+ " , " + "Level of heat " + " , " + appliances[3].levelOfHeat);
		//System.out.println("The temperature of your clothing iron is " + appliances[0].getChangeTemp());
		appliances[0].getIronClothes();
		appliances[1].getRecharge();
	}
}